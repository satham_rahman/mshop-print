package com.app.printmshop

import android.annotation.SuppressLint
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView

class BluetoothAdapter(val btLst : List<BTModel>) : RecyclerView.Adapter<BluetoothAdapter.ViewHolder>(){
    private val TAG = "BluetoothAdapter"
    var onItemClick: ((BTModel) -> Unit)? = null
    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        var cardView : CardView = itemView.findViewById(R.id.cardView);
        val name: TextView = itemView.findViewById(R.id.name)
        val address: TextView = itemView.findViewById(R.id.address)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_bt_row, parent, false)
        return ViewHolder(view)
    }

    @SuppressLint("ResourceAsColor")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val BTModel = btLst[position]

        // sets the image to the imageview from our itemHolder class
        holder.name.setText(BTModel.name)
        holder.address.setText(BTModel.address)
        holder.cardView.setOnClickListener {
            Log.d(TAG, String.format("onBindViewHolder: %s", BTModel.name))
            onItemClick?.invoke(BTModel)
        }
  }

    override fun getItemCount(): Int {
        return btLst.size
    }
}

