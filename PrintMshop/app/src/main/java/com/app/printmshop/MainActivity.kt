package com.app.printmshop

import android.bluetooth.BluetoothDevice
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.hoinprinterlib.HoinPrinter
import com.example.hoinprinterlib.module.PrinterCallback
import com.example.hoinprinterlib.module.PrinterEvent
import java.lang.Exception
import com.app.printmshop.BTModel as BTModel

class MainActivity() : AppCompatActivity() {
    var mHoinPrinter : HoinPrinter? = null
    var devices : Set<BluetoothDevice> = emptySet()
    var selectedPrinterAddress : String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val recyclerview = findViewById<RecyclerView>(R.id.blueToothLst)
        val selectedPrinterTxt = findViewById<TextView>(R.id.selectedPrinter)
        recyclerview.layoutManager = LinearLayoutManager(this)
        val printBtn =  findViewById<Button>(R.id.printBtn);
        printBtn.setOnClickListener {
            print("Button clicked")
            if (devices.size > 0) {
                mHoinPrinter?.connect(selectedPrinterAddress)
                mHoinPrinter?.printText(
                    "Test mshop print",
                    true,
                    true,
                    true,
                    true
                )
            }else{
                showToast("No paired devices found")
            }
        }


        val printerCallback = object : PrinterCallback{
            override fun onState(p0: Int) {
                when(p0){
                    0 -> showToast("BLUETOOTH DISCONNECTED")
                    1 -> showToast("BT_STATE_LISTEN")
                    2 -> showToast("BT-Connecting")
                    3 -> showToast("BT-Connected")
                }
            }

            override fun onError(p0: Int) {
                when(p0){
                    1000 -> showToast("BT-NOT_AVAILABLE")
                    1001 -> showToast("BT-UNABLE TO CONNECT TO DEVICE")
                    1002 -> showToast("BT-CONNECTION LOST")
                    1003 -> showToast("Context Error")

                }
            }

            override fun onEvent(p0: PrinterEvent?) {
                if (p0 != null) {
                    when(p0.event){
                        1 -> showToast("BT Device Found")
                        2 -> showToast("BT Search Completed")
                        3 -> showToast("Receiving byte")
                    }
                }
            }

        }
        try {
            mHoinPrinter = HoinPrinter.getInstance(this@MainActivity, 1, printerCallback)
            mHoinPrinter?.startBtDiscovery()
        }catch ( ex  : Exception){
            Log.d("test", ex.localizedMessage)
        }

        devices = mHoinPrinter?.pairedDevice as Set<BluetoothDevice>;
        var data = ArrayList<BTModel>()
        for (device in devices ){
            var model =  BTModel(name = device.name, address = device.address)
            data.add(model)
        }
        mHoinPrinter?.stopBtDiscovery()

        val adapter = BluetoothAdapter(data)
        adapter.onItemClick = {
            it.address?.let { it1 -> Log.d("TAG", it1)
                selectedPrinterAddress = it1
                selectedPrinterTxt.setText(String.format("Selected printer : %s", it1))
            }
        }
        recyclerview.adapter = adapter
    }

    override  fun onDestroy() {
        super.onDestroy()
        mHoinPrinter?.destroy()
    }

    fun showToast(msg : String){
        Toast.makeText(applicationContext,msg,Toast.LENGTH_SHORT).show()
    }
}
